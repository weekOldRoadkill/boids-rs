// Imports
use ggez::glam::*;

// Weights Type
pub struct Weights {
    pub separation: f32,
    pub alignment: f32,
    pub cohesion: f32,
    pub target: f32,
}

impl Default for Weights {
    fn default() -> Self {
        Self {
            cohesion: 200.,
            separation: 1.,
            alignment: 3.,
            target: 4.,
        }
    }
}

// Range Type
pub struct Range {
    pub range: f32,
    pub sqr: f32,
}

impl Range {
    pub fn new(range: f32) -> Self {
        Range {
            sqr: range * range,
            range,
        }
    }
}

impl Default for Range {
    fn default() -> Self {
        Self::new(0.4)
    }
}

// Visibility Type
pub struct Visibility {
    pub range: bool,
    pub body: bool,
    pub tail: bool,
    pub ui: bool,
}

impl Default for Visibility {
    fn default() -> Self {
        Self {
            range: true,
            body: true,
            tail: true,
            ui: true,
        }
    }
}

// Settings Type
#[derive(Default)]
pub struct Settings {
    pub weights: Weights,
    pub vis: Visibility,
    pub range: Range,
}
