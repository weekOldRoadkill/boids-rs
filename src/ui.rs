// Imports
use crate::game::*;
use ggez::{glam::*, graphics::*, *};

// Selector Type
#[derive(PartialEq, Default, Clone, Copy, Eq)]
pub struct Selector(isize);

impl Selector {
    pub const RANGE: Self = Self(0);
    pub const SEPARATION: Self = Self(1);
    pub const ALIGNMENT: Self = Self(2);
    pub const COHESION: Self = Self(3);
    pub const TARGET: Self = Self(4);

    pub fn next(self: Self) -> Self {
        Self((self.0 + 1).rem_euclid(5))
    }

    pub fn prev(self: Self) -> Self {
        Self((self.0 - 1).rem_euclid(5))
    }
}

// UI Type
#[derive(Default)]
pub struct UI {
    pub selector: Selector,
}

impl UI {
    pub fn draw(self: &Self, ctx: &Context, canvas: &mut Canvas, game: &Game) -> GameResult {
        let text = {
            let selected = Color::new(0.6, 0.6, 0.6, 1.);
            let mut text = Text::default();

            text.add(TextFragment::new(format!(
                "Frame Rate: {:.0}\n",
                ctx.time.fps()
            )))
            .add(TextFragment {
                text: format!("Range: {:.2}\n", game.settings.range.range),
                color: if self.selector == Selector::RANGE {
                    Some(selected)
                } else {
                    None
                },
                ..TextFragment::default()
            })
            .add(TextFragment {
                text: format!(
                    "Separation Weight: {:.2}\n",
                    game.settings.weights.separation
                ),
                color: if self.selector == Selector::SEPARATION {
                    Some(selected)
                } else {
                    None
                },
                ..TextFragment::default()
            })
            .add(TextFragment {
                text: format!("Alignment Weight: {:.2}\n", game.settings.weights.alignment),
                color: if self.selector == Selector::ALIGNMENT {
                    Some(selected)
                } else {
                    None
                },
                ..TextFragment::default()
            })
            .add(TextFragment {
                text: format!("Cohesion Weight: {:.2}\n", game.settings.weights.cohesion),
                color: if self.selector == Selector::COHESION {
                    Some(selected)
                } else {
                    None
                },
                ..TextFragment::default()
            })
            .add(TextFragment {
                text: format!("Target Weight: {:.2}\n", game.settings.weights.target),
                color: if self.selector == Selector::TARGET {
                    Some(selected)
                } else {
                    None
                },
                ..TextFragment::default()
            })
            .set_scale(PxScale::from(28.));
            text
        };

        canvas.draw(
            &Quad,
            DrawParam::new()
                .dest(Vec2::splat(5.))
                .scale(Vec2::from(text.measure(ctx)?) + 10.)
                .color(Color::new(0.2, 0.2, 0.2, 0.7)),
        );
        canvas.draw(
            &text,
            DrawParam::new()
                .dest(Vec2::splat(10.))
                .color(Color::new(0.9, 0.9, 0.9, 1.)),
        );
        Ok(())
    }
}
