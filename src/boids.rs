// Imports
use crate::{settings::*, utils::*, *};
use ggez::{glam::*, graphics::*, *};
use rand::prelude::*;
use rayon::prelude::*;
use rstar::*;

// Boid Type
#[derive(Clone)]
pub struct Boid {
    id: usize,
    pos: Vec2,
    dir: Vec2,
    vel: f32,
}

impl Boid {
    fn new(pos: Vec2, dir: Vec2, vel: f32) -> Self {
        Self {
            id: new_id(),
            dir,
            pos,
            vel,
        }
    }

    pub fn mesh(ctx: &Context, settings: &Settings) -> GameResult<Mesh> {
        let mut mesh = MeshBuilder::new();
        if settings.vis.body {
            mesh.circle(DrawMode::fill(), Vec2::default(), 0.02, 0.01, Color::WHITE)?;
        }
        if settings.vis.tail {
            mesh.line(
                &[Vec2::default(), Vec2::new(-0.075, 0.)],
                0.01,
                Color::WHITE,
            )?;
        }
        if settings.vis.range {
            mesh.circle(
                DrawMode::fill(),
                Vec2::default(),
                settings.range.range,
                0.01,
                Color::new(1., 1., 1., 0.0025),
            )?;
        }
        Ok(Mesh::from_data(ctx, mesh.build()))
    }
}

impl RTreeObject for Boid {
    type Envelope = AABB<(f32, f32)>;

    fn envelope(self: &Self) -> Self::Envelope {
        AABB::from_point(self.pos.into())
    }
}

impl PointDistance for Boid {
    fn distance_2(self: &Self, point: &(f32, f32)) -> f32 {
        self.pos.distance_squared(Vec2::from(*point))
    }
}

// Boids Type
pub struct Boids(RTree<Boid>);

impl Boids {
    pub fn new(map: &MapInfo, size: usize) -> Self {
        let mut gen = thread_rng();
        Boids(RTree::bulk_load(
            (0..size)
                .map(|_| {
                    Boid::new(
                        random_vec(&map.range),
                        random_dir(),
                        gen.gen_range(0.9..1.1),
                    )
                })
                .collect(),
        ))
    }

    pub fn update(
        self: &mut Self,
        delta: f32,
        settings: &Settings,
        map: &MapInfo,
        target: Option<Vec2>,
    ) -> () {
        self.0 = RTree::bulk_load(
            self.0
                .iter()
                .par_bridge()
                .map(|boid| {
                    let mut boid = boid.clone();

                    let (separation, mut alignment, mut cohesion, close_boids) = self
                        .0
                        .locate_within_distance(boid.pos.into(), settings.range.sqr)
                        .fold(
                            (Vec2::default(), Vec2::default(), Vec2::default(), 0),
                            |acc, other_boid| {
                                if boid.id != other_boid.id {
                                    let (separation, alignment, cohesion, close_boids) = acc;
                                    let rel_pos = boid.pos - other_boid.pos;
                                    let dist_sqr = rel_pos.length_squared();
                                    (
                                        separation
                                            + if dist_sqr == 0. {
                                                random_dir()
                                            } else {
                                                rel_pos / dist_sqr.sqrt()
                                            },
                                        alignment + other_boid.dir,
                                        cohesion - rel_pos,
                                        close_boids + 1,
                                    )
                                } else {
                                    acc
                                }
                            },
                        );
                    if close_boids != 0 {
                        alignment /= close_boids as f32;
                        cohesion /= close_boids as f32;
                    }

                    let target_dir = target
                        .map_or_else(
                            || {
                                let mut target_dir = Vec2::default();
                                if !(map.range.start.x..map.range.end.x).contains(&boid.pos.x) {
                                    target_dir.x -= boid.pos.x;
                                }
                                if !(map.range.start.y..map.range.end.y).contains(&boid.pos.y) {
                                    target_dir.y -= boid.pos.y;
                                }
                                target_dir
                            },
                            |target| target - boid.pos,
                        )
                        .normalize_or_zero();

                    boid.dir = (boid.dir
                        + delta
                            * (settings.weights.separation * separation
                                + settings.weights.alignment * alignment
                                + settings.weights.cohesion * cohesion
                                + settings.weights.target * target_dir))
                        .normalize();
                    boid.pos += delta * boid.vel * boid.dir;
                    boid
                })
                .collect(),
        );
    }

    pub fn draw(self: &Self, ctx: &Context, canvas: &mut Canvas, mesh: &Mesh) -> () {
        let mut instances = InstanceArray::new(ctx, None);
        instances.set(self.0.iter().map(|boid| {
            DrawParam::new()
                .dest(boid.pos)
                .rotation(boid.dir.y.atan2(boid.dir.x))
        }));
        canvas.draw_instanced_mesh(mesh.clone(), &instances, DrawParam::new());
    }
}
