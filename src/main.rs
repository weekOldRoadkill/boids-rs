// Imports
mod boids;
mod game;
mod map;
mod settings;
mod ui;
mod utils;

use game::*;
use ggez::{conf::*, event::*, *};
use map::*;

// Main Function
fn main() -> GameResult {
    let (ctx, event_loop) = ContextBuilder::new("boids", "weekOldRoadkill")
        .window_mode(WindowMode::default().maximized(true).resizable(true))
        .window_setup(
            WindowSetup::default()
                .samples(NumSamples::Four)
                .title("Boids")
                .vsync(false),
        )
        .backend(if cfg!(windows) {
            Backend::Dx12
        } else {
            Backend::default()
        })
        .build()?;
    ctx.gfx.window().focus_window();
    let game = Game::new(&ctx)?;
    run(ctx, event_loop, game);
}
