// Imports
use crate::{boids::*, input::keyboard::*, map::*, settings::*, ui::*, utils::*};
use ggez::{event::*, glam::*, graphics::*, *};

// Game Type
pub struct Game {
    pub settings: Settings,
    target: Option<Vec2>,
    boid_mesh: Mesh,
    boids: Boids,
    map: MapInfo,
    ui: UI,
}

impl Game {
    pub fn new(ctx: &Context) -> GameResult<Self> {
        let map = MapInfo::new(ctx, Vec2::new(argument(2, 20.), argument(3, 15.)));
        let settings = Settings::default();

        Ok(Self {
            boids: Boids::new(&map, argument(1, 10000)),
            boid_mesh: Boid::mesh(ctx, &settings)?,
            ui: UI::default(),
            target: None,
            settings,
            map,
        })
    }
}

// Game Events
impl EventHandler for Game {
    fn update(self: &mut Self, ctx: &mut Context) -> GameResult {
        let delta = ctx.time.delta().as_secs_f32();
        self.boids
            .update(delta, &self.settings, &self.map, self.target);
        Ok(())
    }

    fn draw(self: &mut Self, ctx: &mut Context) -> GameResult {
        let mut canvas = Canvas::from_frame(ctx, Color::new(0.05, 0.05, 0.05, 1.));
        let default_proj = canvas.projection();

        canvas.set_screen_coordinates(self.map.coords);
        canvas.set_scissor_rect(rect_from_vecs_p2p(
            self.map.world_to_screen(ctx, self.map.range.start),
            self.map.world_to_screen(ctx, self.map.range.end),
        ))?;
        self.boids.draw(ctx, &mut canvas, &self.boid_mesh);
        canvas.set_default_scissor_rect();
        canvas.set_projection(default_proj);

        if self.settings.vis.ui {
            self.ui.draw(ctx, &mut canvas, self)?;
        }
        canvas.finish(ctx)?;
        Ok(())
    }

    fn resize_event(self: &mut Self, ctx: &mut Context, _width: f32, _height: f32) -> GameResult {
        self.map = MapInfo::new(ctx, self.map.size);
        Ok(())
    }

    fn mouse_motion_event(
        self: &mut Self,
        ctx: &mut Context,
        x: f32,
        y: f32,
        _dx: f32,
        _dy: f32,
    ) -> GameResult {
        self.target = self
            .target
            .map(|_| self.map.screen_to_world(ctx, Vec2::new(x, y)));
        Ok(())
    }

    fn mouse_button_down_event(
        self: &mut Self,
        ctx: &mut Context,
        button: MouseButton,
        x: f32,
        y: f32,
    ) -> GameResult {
        match button {
            MouseButton::Left => {
                self.target = Some(self.map.screen_to_world(ctx, Vec2::new(x, y)));
            }
            _ => (),
        }
        Ok(())
    }

    fn mouse_button_up_event(
        self: &mut Self,
        _ctx: &mut Context,
        button: MouseButton,
        _x: f32,
        _y: f32,
    ) -> GameResult {
        match button {
            MouseButton::Left => self.target = None,
            _ => (),
        }
        Ok(())
    }

    fn key_down_event(
        self: &mut Self,
        ctx: &mut Context,
        input: KeyInput,
        _repeated: bool,
    ) -> GameResult {
        let mut map_selected = |f: fn(f32) -> f32| {
            match self.ui.selector {
                Selector::TARGET => self.settings.weights.target = f(self.settings.weights.target),
                Selector::SEPARATION => {
                    self.settings.weights.separation = f(self.settings.weights.separation);
                }
                Selector::ALIGNMENT => {
                    self.settings.weights.alignment = f(self.settings.weights.alignment);
                }
                Selector::COHESION => {
                    self.settings.weights.cohesion = f(self.settings.weights.cohesion);
                }
                Selector::RANGE => {
                    let range = f(self.settings.range.range);
                    self.boid_mesh = Boid::mesh(ctx, &self.settings)?;
                    self.settings.range = Range::new(range);
                }
                _ => (),
            }
            Ok(()) as GameResult
        };

        match input.keycode {
            Some(KeyCode::Down) => {
                self.ui.selector = self.ui.selector.next();
            }
            Some(KeyCode::Up) => {
                self.ui.selector = self.ui.selector.prev();
            }
            Some(KeyCode::Left) => {
                map_selected(|a| 0.8 * a)?;
            }
            Some(KeyCode::Right) => {
                map_selected(|a| 1.25 * a)?;
            }
            Some(KeyCode::R) => {
                self.settings = Settings::default();
                self.boid_mesh = Boid::mesh(ctx, &self.settings)?;
            }
            Some(KeyCode::T) => {
                self.settings.vis.ui = !self.settings.vis.ui;
            }
            Some(KeyCode::Y) => {
                self.settings.vis.range = !self.settings.vis.range;
                self.boid_mesh = Boid::mesh(ctx, &self.settings)?;
            }
            Some(KeyCode::U) => {
                self.settings.vis.body = !self.settings.vis.body;
                self.boid_mesh = Boid::mesh(ctx, &self.settings)?;
            }
            Some(KeyCode::I) => {
                self.settings.vis.tail = !self.settings.vis.tail;
                self.boid_mesh = Boid::mesh(ctx, &self.settings)?;
            }
            _ => (),
        }
        Ok(())
    }
}
