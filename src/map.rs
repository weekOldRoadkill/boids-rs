// Imports
use crate::utils::*;
use ggez::{glam::*, graphics::*, *};

// Map Info Type
pub struct MapInfo {
    pub range: std::ops::Range<Vec2>,
    pub coords: Rect,
    pub size: Vec2,
}

impl MapInfo {
    pub fn new(ctx: &Context, size: Vec2) -> Self {
        let dims = Vec2::from(ctx.gfx.drawable_size());
        let half_size = 0.5 * size;
        let scale = dims / (dims / half_size).min_element();
        Self {
            coords: rect_from_vecs(-scale, 2. * scale),
            range: -half_size..half_size,
            size,
        }
    }

    pub fn screen_to_world(self: &Self, ctx: &Context, a: Vec2) -> Vec2 {
        Vec2::from(self.coords.size()) * a / Vec2::from(ctx.gfx.drawable_size())
            + Vec2::from(self.coords.point())
    }

    pub fn world_to_screen(self: &Self, ctx: &Context, a: Vec2) -> Vec2 {
        Vec2::from(ctx.gfx.drawable_size()) * (a - Vec2::from(self.coords.point()))
            / Vec2::from(self.coords.size())
    }
}
