// Imports
use ggez::{glam::*, graphics::*};
use rand::prelude::*;

// New ID Function
static ID_COUNTER: std::sync::atomic::AtomicUsize = std::sync::atomic::AtomicUsize::new(0);
pub fn new_id() -> usize {
    ID_COUNTER.fetch_add(1, std::sync::atomic::Ordering::Relaxed)
}

// Random Direction Vector Function
pub fn random_dir() -> Vec2 {
    Vec2::from_angle(thread_rng().gen_range(0. ..std::f32::consts::TAU))
}

// Random Vector Function
pub fn random_vec(range: &std::ops::Range<Vec2>) -> Vec2 {
    let mut gen = thread_rng();
    Vec2::new(
        gen.gen_range(range.start.x..range.end.x),
        gen.gen_range(range.start.y..range.end.y),
    )
}

// Argument Function
pub fn argument<T: std::str::FromStr>(n: usize, default: T) -> T {
    std::env::args()
        .nth(n)
        .and_then(|arg| arg.parse().ok())
        .unwrap_or(default)
}

// Rectangle from Vectors Function
pub fn rect_from_vecs(point: Vec2, size: Vec2) -> Rect {
    Rect::new(point.x, point.y, size.x, size.y)
}

// Rectangle from Vectors Function (point to point)
pub fn rect_from_vecs_p2p(a: Vec2, b: Vec2) -> Rect {
    rect_from_vecs(a, b - a)
}
